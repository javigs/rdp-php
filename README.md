### PHP implementation of Ramer–Douglas–Peucker algorithm.

The purpose of the algorithm is, given a curve composed of line segments (which is also called a Polyline in some contexts), to find a similar curve with fewer points.

Reduce points:

![Algorithm rdp](images/Douglas-Peucker_animated.gif)

Parameter epsilon precision effect:

![RDP_varying_epsilon](images/RDP_varying_epsilon.gif)

-------------------

+Info: https://en.wikipedia.org/wiki/Ramer-Douglas-Peucker_algorithm
