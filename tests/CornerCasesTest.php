<?php

use javigs\rdp_php\shapes\Point;
use javigs\rdp_php\RamerDouglasPeucker;
use PHPUnit\Framework\TestCase;

class CornerCasesTest extends TestCase
{
    /** @test */
    public function test_when_empty_points_then_returns_empty_array()
    {
        $result = RamerDouglasPeucker::reduce([], 0.5);
        $this->assertEquals([], $result);
    }

    /** @test */
    public function test_when_one_point_then_returns_same_point()
    {
        $poly = [new Point(1.0, 2.0)];
        $result = RamerDouglasPeucker::reduce($poly, 0.5);
        $this->assertCount(1, $result);
        $this->assertEquals($poly[0]->toArray(), $result[0]->toArray());
    }

    /** @test */
    public function test_when_two_points_then_returns_two_points_array()
    {
        $poly = [new Point(1.0, 2.0), new Point(3.0, 5.0)];
        $result = RamerDouglasPeucker::reduce($poly, 0.5);
        $this->assertCount(2, $result);
        $this->assertEquals($poly[0]->toArray(), $result[0]->toArray());
        $this->assertEquals($poly[1]->toArray(), $result[1]->toArray());
    }

    /** @test */
    public function test_when_horizontal_line_then_returns_first_and_last_point_array()
    {
        $horizontal_line = [
            new Point(1.0, 2.0),
            new Point(2.0, 2.0),
            new Point(3.0, 2.0),
            new Point(4.0, 2.0),
            new Point(5.0, 2.0)
        ];
        $result = RamerDouglasPeucker::reduce($horizontal_line, 0.5);

        $expected_result = [
            new Point(1.0, 2.0),
            new Point(5.0, 2.0)
        ];
        $this->assertCount(2, $result);
        $this->assertEquals($expected_result[0]->toArray(), $result[0]->toArray());
        $this->assertEquals($expected_result[1]->toArray(), $result[1]->toArray());
    }

    /** @test */
    public function test_when_vertical_line_then_returns_first_and_last_point_array()
    {
        $vertical_line = [
            new Point(2.0, 1.0),
            new Point(2.0, 2.0),
            new Point(2.0, 3.0),
            new Point(2.0, 4.0),
            new Point(2.0, 5.0),
            new Point(2.0, 6.0)
        ];
        $result = RamerDouglasPeucker::reduce($vertical_line, 0.5);

        $expected_result = [
            new Point(2.0, 1.0),
            new Point(2.0, 6.0)
        ];
        $this->assertCount(2, $result);
        $this->assertEquals($expected_result[0]->toArray(), $result[0]->toArray());
        $this->assertEquals($expected_result[1]->toArray(), $result[1]->toArray());
    }
}
