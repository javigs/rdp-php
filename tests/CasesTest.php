<?php

use javigs\rdp_php\shapes\Point;
use javigs\rdp_php\RamerDouglasPeucker;
use PHPUnit\Framework\TestCase;

class CasesTest extends TestCase
{
    /** @test */
    public function test_case1()
    {
        $poly = [
            new Point(150, 10),
            new Point(200, 100),
            new Point(360, 170),
            new Point(500, 280)
        ];
        $expected_result = [
            new Point(150, 10),
            new Point(200, 100),
            new Point(500, 280)
        ];
        $result = RamerDouglasPeucker::reduce($poly, 30);
        $this->assertCount(3, $result);
        $this->assertSamePoints($expected_result, $result);
    }

    /** @test */
    public function test_case2()
    {
        $poly = [
            new Point(25, 25),
            new Point(37, 62),
            new Point(75, 75),
            new Point(100, 100)
        ];

        $expected_result = [
            new Point(25, 25),
            new Point(37, 62),
            new Point(100, 100)
        ];
        $result = RamerDouglasPeucker::reduce($poly, 10);
        $this->assertCount(3, $result);
        $this->assertSamePoints($expected_result, $result);

        $expected_result = [
            new Point(25, 25),
            new Point(100, 100)
        ];
        $result = RamerDouglasPeucker::reduce($poly, 20);
        $this->assertCount(2, $result);
        $this->assertSamePoints($expected_result, $result);
    }

    /** @test */
    public function test_case3()
    {
        $poly = [
            new Point(-30, -40),
            new Point(-20, -10),
            new Point(10, 10),
            new Point(50, 0),
            new Point(40, -30),
            new Point(10, -40)
        ];

        $expected_result = [
            new Point(-30, -40),
            new Point(10, 10),
            new Point(50, 0),
            new Point(40, -30),
            new Point(10, -40)
        ];
        $result = RamerDouglasPeucker::reduce($poly, 12);
        $this->assertCount(5, $result);
        $this->assertSamePoints($expected_result, $result);

        $expected_result = [
            new Point(-30, -40),
            new Point(10, 10),
            new Point(50, 0),
            new Point(10, -40)
        ];
        $result = RamerDouglasPeucker::reduce($poly, 15);
        $this->assertCount(4, $result);
        $this->assertSamePoints($expected_result, $result);

        $expected_result = [
            new Point(-30, -40),
            new Point(10, 10),
            new Point(10, -40)
        ];
        $result = RamerDouglasPeucker::reduce($poly, 40);
        $this->assertCount(3, $result);
        $this->assertSamePoints($expected_result, $result);

        $expected_result = [
            new Point(-30, -40),
            new Point(10, -40)
        ];
        $result = RamerDouglasPeucker::reduce($poly, 50);
        $this->assertCount(2, $result);
        $this->assertSamePoints($expected_result, $result);
    }

    /** @test */
    public function test_case4()
    {
        $poly = [
            new Point(0.0034, 0.013),
            new Point(0.0048, 0.006),
            new Point(0.0062, 0.01),
            new Point(0.0087, 0.009)
        ];

        $result = RamerDouglasPeucker::reduce($poly, 0.001);
        $this->assertCount(4, $result);
        $this->assertSamePoints($poly, $result);

        $expected_result = [
            new Point(0.0034, 0.013),
            new Point(0.0048, 0.006),
            new Point(0.0087, 0.009)
        ];
        $result = RamerDouglasPeucker::reduce($poly, 0.003);
        $this->assertCount(3, $result);
        $this->assertSamePoints($expected_result, $result);

        $expected_result = [
            new Point(0.0034, 0.013),
            new Point(0.0087, 0.009)
        ];
        $result = RamerDouglasPeucker::reduce($poly, 0.01);
        $this->assertCount(2, $result);
        $this->assertSamePoints($expected_result, $result);
    }

    /** @test */
    public function test_case5()
    {
        $poly = [
            new Point(-43.0, 8.0),
            new Point(-24.0, 19.0),
            new Point(-13.0, 23.0),
            new Point(-8.0, 36.0),
            new Point(7.0, 40.0),
            new Point(24.0, 12.0),
            new Point(44.0, -6.0),
            new Point(57.0, 2.0),
            new Point(70.0, 7.0)
        ];

        $expected_result = [
            $poly[0],
            $poly[4],
            $poly[6],
            $poly[8]
        ];

        $result = RamerDouglasPeucker::reduce($poly, 10);
        $this->assertCount(4, $result);
        $this->assertSamePoints($expected_result, $result);
    }

    private function assertSamePoints(array $points1, array $points2)
    {
        $this->assertFalse(count($points1) != count($points2));
        $this->assertGreaterThan(0, count($points1));
        for ($i=0 ; $i<count($points1) ; $i++) {
            $this->assertEquals($points1[1]->toArray(), $points2[1]->toArray());
        }
    }
}
