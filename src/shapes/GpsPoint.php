<?php

namespace javigs\rdp_php\shapes;

/**
 * This is an example of extended use of Point.
 * Algorithm only takes care of base Point x and y values
 */
class GpsPoint extends Point
{
    /** EXTRA DATA */
    /** @var string  - datetime, number sequence, etc. */
    private $sequence;
    /** @var float - meters*/
    private $elevation;
    /** @var int - meters */
    private $distance;
    /** @var float - km/h */
    private $speed;

    public function __construct(string $sequence, float $lng, float $lat, float $elevation, int $distance, float $speed)
    {
        parent::__construct($lng, $lat);
        $this->sequence = $sequence;
        $this->elevation = $elevation;
        $this->distance = $distance;
        $this->speed = $speed;
    }

    public function getLng(): float
    {
        return $this->getX();
    }

    public function getLat(): float
    {
        return $this->getY();
    }

    public function getSequence(): string
    {
        return $this->sequence;
    }

    public function getElevation(): float
    {
        return $this->elevation;
    }

    public function getDistance(): int
    {
        return $this->distance;
    }

    public function getSpeed(): float
    {
        return $this->speed;
    }

    public function toArray(): array
    {
        return [
            'lat' => $this->getLat(),
            'lng' => $this->getLng(),
            'sequence' => $this->getSequence(),
            'elevation' => $this->getElevation(),
            'distance' => $this->getDistance(),
            'speed' => $this->getSpeed()
        ];
    }
}
