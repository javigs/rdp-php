<?php

namespace javigs\rdp_php\shapes;

class Line
{
    /** @var Point */
    private $point1;
    /** @var Point */
    private $point2;
    /** @var float */
    private $slope;
    /** @var float */
    private $intersection;

    public function __construct(Point $point1, Point $point2)
    {
        $this->point1 = $point1;
        $this->point2 = $point2;
        $this->slope = $this->calcSlope();
        $this->intersection = $this->calcIntersectionY();
    }

    private function calcSlope(): float
    {
        $float_epsilon = defined('PHP_FLOAT_EPSILON') ? PHP_FLOAT_EPSILON : 1.0E-16;
        if (abs($this->point2->getX() - $this->point1->getX()) < $float_epsilon) {
            if ($this->point2->getX() > $this->point1->getX()) {
                return INF;
            } else {
                return -INF;
            }
        }

        return ($this->point2->getY() - $this->point1->getY()) / ($this->point2->getX() - $this->point1->getX());
    }

    private function calcIntersectionY(): float
    {
        return $this->point1->getY() - ($this->slope * $this->point1->getX());
    }

    public function getPoint1(): Point
    {
        return $this->point1;
    }

    public function getPoint2(): Point
    {
        return $this->point2;
    }

    public function getSlope(): float
    {
        return $this->slope;
    }

    public function getIntersection(): float
    {
        return $this->intersection;
    }
}
