<?php

namespace javigs\rdp_php;

use javigs\rdp_php\shapes\Line;
use javigs\rdp_php\shapes\Point;

class RamerDouglasPeucker {
    /**
     * @param array $point_list - array of Points
     * @param float $epsilon - Tolerance distance to line reference
     * @return array - array of Points
     */
    public static function reduce(array $point_list, float $epsilon): array
    {
        if (count($point_list) < 3) {
            return $point_list;
        }

        $bigger_distance = 0;
        $index = 0;
        $end = count($point_list) - 1;
        $line = new Line($point_list[0], $point_list[$end]);
        for ($i = 1 ; $i < $end; $i++) {
            $distance = self::perpendicularDistance($point_list[$i], $line);
            if ($distance > $bigger_distance) {
                $index = $i;
                $bigger_distance = $distance;
            }
        }

        if ($bigger_distance > $epsilon) {
            $recResults1 = self::reduce(array_slice($point_list, 0, $index + 1), $epsilon);
            $recResults2 = self::reduce(array_slice($point_list, $index), $epsilon);

            // concat $recResults2 to $recResults1 minus the end/start point that will be the same
            $result_list = array_merge(array_slice($recResults1, 0, count($recResults1) - 1), $recResults2);
        } else {
            $result_list = [ $point_list[0], $point_list[$end] ];
        }

        return $result_list;
    }

    private static function perpendicularDistance(Point $point, Line $line): float
    {
        $slope = $line->getSlope();
        if (is_infinite($slope)) {
            //vertical lines
            return abs($point->getX() - $line->getPoint1()->getX());
        }

        return abs(($slope * $point->getX()) - $point->getY() + $line->getIntersection())
            / sqrt($slope * $slope + 1);
    }
}
