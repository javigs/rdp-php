<?php
require('../vendor/autoload.php');

use javigs\rdp_php\RamerDouglasPeucker;
use javigs\rdp_php\shapes\GpsPoint;

const TOLERANCE = 0.00001;

$file = fopen('activity_66308030.tsv', 'r');
$gps_points = [];
if (is_resource($file)) {
    //Skip header
    fgets($file);
    while (($line = fgetcsv($file, 0, "\t")) !== false) {
        if (is_numeric($line[2]) && is_numeric($line[1])) {
            $gps_points[] = new GpsPoint($line[0], $line[2], $line[1], (float)$line[3], (int)$line[5], (float)$line[4]);
        }
    }
    fclose($file);
}

printf("INPUT\n");
printf("- File points: %d\n", count($gps_points));
printf("- Tolerance selected: %f\n", TOLERANCE);

$elapsed_time = microtime(true);
$optimized_points = RamerDouglasPeucker::reduce($gps_points, TOLERANCE);
$elapsed_time = microtime(true) - $elapsed_time;

printf("RESULT:\n");
printf("- Optimized points: %d\n", count($optimized_points));
printf("- Removed points: %d\n", count($gps_points) - count($optimized_points));
printf("- Elapsed time: %lfs\n\n", $elapsed_time);
